//
//  NSArray+Util.m
//  Account
//
//  Created by Tracy on 1/18/16.
//  Copyright © 2016 Tracy. All rights reserved.
//

#import "NSArray+Util.h"

@implementation NSArray (Util)

- (NSArray*)filter:(NSArray*)values {
    NSMutableArray *copy = [self mutableCopy];
    [copy removeObjectsInArray:values];
    return copy;
}

- (BOOL)objectsContainedByArray:(NSArray*)array {
    for (id item in self) {
        if ([array containsObject:item] == NO) {
            return NO;
        }
    }
    
    return YES;
}

- (NSDictionary*)group:(id (^)(id item))enumBlock {
    NSMutableDictionary *r = [NSMutableDictionary dictionary];
    for (id obj in self) {
        id key = enumBlock(obj);
        if (r[key] == nil) {
            r[key] = [NSMutableArray array];
        }
        
        [r[key] addObject:obj];
    }
    
    return r;
}

- (id)reduceWithInitialValue:(id)initialValue enumBlock:(id (^)(id previousValue, id item, NSInteger index))aBlock {
    id previousValue = initialValue;
    for (NSInteger i=0; i<self.count; i++) {
        previousValue = aBlock(previousValue, self[i], i);
    }
    
    return previousValue;
}

- (id)objectAtIndexSafely:(NSUInteger)index {
    if (index + 1 > self.count) {
        return nil;
    }
    
    return [self objectAtIndex:index];
}

@end
