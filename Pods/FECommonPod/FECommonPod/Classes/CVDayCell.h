//
//  CVDayCell.h
//  Accounts
//
//  Created by Tracy on 15/9/21.
//  Copyright (c) 2015年 Tracy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    CVDayCellStateRegular,
    CVDayCellStateFadeout,
    CVDayCellStateSelected,
    CVDayCellStateInvisible
} CVDayCellState;

@interface CVDayCell : UICollectionViewCell

@property (nonatomic, readonly) UILabel *textLabel;
@property (nonatomic, readonly) UIView *todaySymbol;
@property (nonatomic) CVDayCellState state;

@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *fadeoutTextColor;
@property (nonatomic, strong) UIColor *highlightedTextColor;
@property (nonatomic, strong) UIColor *highlightedBackgroundColor;

@end
