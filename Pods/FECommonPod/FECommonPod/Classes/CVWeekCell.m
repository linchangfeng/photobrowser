//
//  CVWeekCell.m
//  Account
//
//  Created by Tracy on 1/12/16.
//  Copyright © 2016 Tracy. All rights reserved.
//

#import "CVWeekCell.h"

@implementation CVWeekCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _weekLabel = [[UILabel alloc] init];
        _weekLabel.textColor = [UIColor blackColor];
        _weekLabel.textAlignment = NSTextAlignmentCenter;
        _weekLabel.font = [UIFont systemFontOfSize:13];
        _weekLabel.adjustsFontSizeToFitWidth = YES;
        [self addSubview:_weekLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _weekLabel.frame = self.bounds;
}

@end
