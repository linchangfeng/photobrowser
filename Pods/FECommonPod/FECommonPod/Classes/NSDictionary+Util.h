//
//  NSDictionary+Util.h
//  Account
//
//  Created by Tracy on 16/2/5.
//  Copyright © 2016年 Tracy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Util)

- (void)sortKeysUsingComparator:(NSComparator)cmptr thenEnumerateKeysAndObjectsUsingBlock:(void (^)(id key, id obj, BOOL *stop))block;

@end
