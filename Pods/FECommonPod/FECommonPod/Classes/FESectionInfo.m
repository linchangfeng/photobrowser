//
//  SectionInfo.m
//  memo
//
//  Created by Tracy on 15/1/23.
//  Copyright (c) 2015年 Tracy. All rights reserved.
//

#import "FESectionInfo.h"

@implementation FESectionInfo

+ (id)infoWithTitle:(NSString *)title items:(NSArray<FECellInfo*>*)items {
    return [[self alloc] initWithTitle:title items:items cells:nil tag:0];
}

+ (id)infoWithTitle:(NSString*)title items:(NSArray<FECellInfo*>*)items tag:(NSInteger)tag {
    return [[self alloc] initWithTitle:title items:items cells:nil tag:tag];
}

+ (id)infoWithTitle:(NSString*)title cells:(NSArray<UITableViewCell*>*)cells {
    return [[self alloc] initWithTitle:title items:nil cells:cells tag:0];
}

+ (id)infoWithTitle:(NSString*)title cells:(NSArray<UITableViewCell*>*)cells tag:(NSInteger)tag {
    return [[self alloc] initWithTitle:title items:nil cells:cells tag:tag];
}

- (id)initWithTitle:(NSString*)title items:(NSArray<FECellInfo*>*)items {
    return [self initWithTitle:title items:items cells:nil tag:0];
}
- (id)initWithTitle:(NSString*)title items:(NSArray<FECellInfo*>*)items tag:(NSInteger)tag {
    return [self initWithTitle:title items:items cells:nil tag:tag];
}
- (id)initWithTitle:(NSString*)title cells:(NSArray<UITableViewCell*>*)cells {
    return [self initWithTitle:title items:nil cells:cells tag:0];
}
- (id)initWithTitle:(NSString*)title cells:(NSArray<UITableViewCell*>*)cells tag:(NSInteger)tag {
    return [self initWithTitle:title items:nil cells:cells tag:tag];
}

- (id)initWithTitle:(NSString *)title items:(NSArray<FECellInfo*>*)items cells:(NSArray<UITableViewCell*>*)cells tag:(NSInteger)tag {
    self = [super init];
    if (self) {
        self.title = title;
        self.items = items;
        self.cells = cells;
        self.tag = tag;
    }
    return self;
}

@end
