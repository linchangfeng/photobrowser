//
//  UINavigationController+Util.h
//  Account
//
//  Created by Tracy on 16/2/1.
//  Copyright © 2016年 Tracy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Util)

@property (nonatomic, readonly) id rootViewController;

@end
