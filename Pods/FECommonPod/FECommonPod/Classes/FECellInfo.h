//
//  CellInfo.h
//  Account
//
//  Created by Tracy on 16/2/27.
//  Copyright © 2016年 Tracy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FECellInfo : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic) NSInteger tag;
@property (nonatomic, strong) id object;
@property (nonatomic, strong) UIImage *icon;

+ (id)infoWithTitle:(NSString*)title tag:(NSInteger)tag;
+ (id)infoWithTitle:(NSString*)title object:(id)object tag:(NSInteger)tag;
+ (id)infoWithTitle:(NSString*)title object:(id)object icon:(UIImage*)icon tag:(NSInteger)tag;
+ (id)infoWithTitle:(NSString*)title icon:(UIImage*)icon tag:(NSInteger)tag;

- (id)initWithTitle:(NSString*)title tag:(NSInteger)tag;
- (id)initWithTitle:(NSString*)title object:(id)object tag:(NSInteger)tag;
- (id)initWithTitle:(NSString*)title object:(id)object icon:(UIImage*)icon tag:(NSInteger)tag;
- (id)initWithTitle:(NSString*)title icon:(UIImage*)icon tag:(NSInteger)tag;

@end
