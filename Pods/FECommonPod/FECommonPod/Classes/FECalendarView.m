//
//  CalendarView.m
//  Account
//
//  Created by Tracy on 1/11/16.
//  Copyright © 2016 Tracy. All rights reserved.
//

#import "FECommonPod.h"
#import "FECalendarView.h"
#import "CVMonthCell.h"

static NSString * const kMonthcellReuseIdentifer = @"Cell";
#define kNumberOfMonths 10000
#define kItemIndexForReferentialMonth (kNumberOfMonths/2)
static NSArray *kWeekdays = nil;

@interface FECalendarView () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CVMonthCellDelegate> {
    UICollectionView *_monthCollectionView;
    
    // 参考月初始化为当前日期
    // 对应的cell显示在collectionView的中心，即indexPath.item == kItemIndexForReferentialMonth
    NSDateComponents *_referentialMonthComponents;
    
    UIButton *_todayButton;
    UILabel *_monthLabel;
}

@end


@implementation FECalendarView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    _referentialMonthComponents = [kCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:[NSDate new]];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _monthTitleColor = FEHexRGBA(0x666666, 1.0);
    _todayButtonTitleColor = FEHexRGBA(0x60c5ba, 1.0);
    
    _dayTextColor = FEHexRGBA(0x666666, 1.0);
    _dayFadeoutTextColor = FEHexRGBA(0xa6a6a5, 1.0);
    _weekTextColor = FEHexRGBA(0xa6a6a5, 1.0);
    _selectedDayTextColor = [UIColor whiteColor];
    _selectedDayBackgroundColor = FEHexRGBA(0x5FC6BB, 1.0);
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    _monthCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    _monthCollectionView.backgroundColor = [UIColor clearColor];
    _monthCollectionView.showsHorizontalScrollIndicator = NO;
    _monthCollectionView.pagingEnabled = YES;
    _monthCollectionView.delegate = (id)self;
    _monthCollectionView.dataSource = (id)self;
    [self addSubview:_monthCollectionView];
    
    [_monthCollectionView registerClass:[CVMonthCell class] forCellWithReuseIdentifier:kMonthcellReuseIdentifer];
    
    _todayButton = [[UIButton alloc] init];
    [_todayButton setTitleColor:_todayButtonTitleColor forState:UIControlStateNormal];
    _todayButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [_todayButton setTitle:_(@"今天") forState:UIControlStateNormal];
    [_todayButton addTarget:self action:@selector(didTapTodayButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_todayButton];
    
    _monthLabel = [[UILabel alloc] init];
    _monthLabel.textColor = _monthTitleColor;
    _monthLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_monthLabel];
    
    [self setCurrentMonthComponents:_referentialMonthComponents];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    _monthLabel.frame = CGRectMake(0, 0, self.width, 50);
    _todayButton.frame = CGRectMake(0, 0, 70, _monthLabel.height);
    
    _monthCollectionView.frame = self.bounds;
    _monthCollectionView.contentInset = UIEdgeInsetsMake(_todayButton.bottom, 0, 0, 0);
    [_monthCollectionView.collectionViewLayout invalidateLayout];
    [self updateContentOffset];
}

#pragma mark - UICollectionView delegate & datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return kNumberOfMonths;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(_monthCollectionView.width, _monthCollectionView.height-_monthCollectionView.contentInset.top);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CVMonthCell *monthCell = [collectionView dequeueReusableCellWithReuseIdentifier:kMonthcellReuseIdentifer forIndexPath:indexPath];
    monthCell.weekTextColor = _weekTextColor;
    monthCell.dayTextColor = _dayTextColor;
    monthCell.dayFadeoutTextColor = _dayFadeoutTextColor;
    monthCell.selectedDayTextColor = _selectedDayTextColor;
    monthCell.selectedDayBackgroundColor = _selectedDayBackgroundColor;
    
    if (monthCell.delegate != self) {
        monthCell.delegate = self;
    }
    
    NSInteger monthDiff = indexPath.item - kItemIndexForReferentialMonth;
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.year = _referentialMonthComponents.year;
    components.month = _referentialMonthComponents.month + monthDiff;
    monthCell.monthComponents = components;
    monthCell.selectedDate = _selectedDate;
    
    return monthCell;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    CVMonthCell *cell = [self cellForCurrentOffset];
    [self updateMonthLabelWithMonthComponents:cell.monthComponents];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CVMonthCell *cell = [self cellForCurrentOffset];
    [self setCurrentMonthComponents:cell.monthComponents];
}

#pragma mark - CVMonthCellDelegate
- (void)monthCell:(CVMonthCell *)monthView didSelectDate:(NSDate *)date {
    _selectedDate = date;
    
    if ([_delegate respondsToSelector:@selector(calendarView:didSelectDate:)]) {
        [_delegate calendarView:self didSelectDate:date];
    }
}

#pragma mark - Handler
- (void)didTapTodayButton:(id)sender {
    NSDate *now = [NSDate new];
    [self setSelectedDate:now];
    
    NSDateComponents *components = [kCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:now];
    [self setCurrentMonthComponents:components];
    
    if ([_delegate respondsToSelector:@selector(calendarView:didJumpToToday:)]) {
        [_delegate calendarView:self didJumpToToday:now];
    }
}

#pragma mark - Private
- (void)updateContentOffset {
    NSDateComponents *components = [kCalendar components:NSCalendarUnitMonth | NSCalendarUnitDay
                                      fromDateComponents:_currentMonthComponents
                                        toDateComponents:_referentialMonthComponents
                                                 options:0];
    
    NSInteger monthDiff = components.month; // _currentMonthComponents - _referentialMonthComponents
    
    _monthCollectionView.contentOffset = CGPointMake(self.width * (kItemIndexForReferentialMonth - monthDiff),
                                                _monthCollectionView.contentOffset.y);
}

- (void)updateTodayButtonVisibility {
    NSDate *date = [kCalendar dateFromComponents:_currentMonthComponents];
    BOOL inSameMonthWithToday = [kCalendar isDate:date equalToDate:[NSDate new] toUnitGranularity:NSCalendarUnitMonth];
    _todayButton.hidden = inSameMonthWithToday;
}

- (void)updateMonthLabelWithMonthComponents:(NSDateComponents*)components {
    NSDate *date = [kCalendar dateFromComponents:components];
    _monthLabel.text = [kUtil stringFromDate:date withFormat:@"MMMM yyyy"];
}

- (CVMonthCell*)cellForCurrentOffset {
    CGFloat offsetX = _monthCollectionView.contentOffset.x;
    NSInteger index = round(offsetX / _monthCollectionView.width);
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    return (CVMonthCell*)[_monthCollectionView cellForItemAtIndexPath:indexPath];
}

#pragma mark - Public
- (void)setSelectedDate:(NSDate *)selectedDate {
    NSDateComponents *components = [kCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:selectedDate];
    _selectedDate = [kCalendar dateFromComponents:components];
    
    for (CVMonthCell *cell in _monthCollectionView.visibleCells) {
        cell.selectedDate = selectedDate;
    }
}

- (void)setCurrentMonthComponents:(NSDateComponents *)currentMonthComponents {
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.year = currentMonthComponents.year;
    components.month = currentMonthComponents.month;
    _currentMonthComponents = components;
    
    [self updateTodayButtonVisibility];
    [self updateContentOffset];
    [self updateMonthLabelWithMonthComponents:_currentMonthComponents];
}

- (void)setTodayButtonTitleColor:(UIColor *)todayButtonTitleColor {
    _todayButtonTitleColor = todayButtonTitleColor;
    [_todayButton setTitleColor:todayButtonTitleColor forState:UIControlStateNormal];
}

- (void)setMonthTitleColor:(UIColor *)monthTitleColor {
    _monthTitleColor = monthTitleColor;
    _monthLabel.textColor = monthTitleColor;
}

@end

