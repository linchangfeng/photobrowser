//
//  CVWeekCell.h
//  Account
//
//  Created by Tracy on 1/12/16.
//  Copyright © 2016 Tracy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVWeekCell : UICollectionViewCell

@property (nonatomic, readonly) UILabel *weekLabel;

@end
