//
//  CalendarViewCell.m
//  Account
//
//  Created by Tracy on 1/11/16.
//  Copyright © 2016 Tracy. All rights reserved.
//

#import "FECommonPod.h"
#import "CVMonthCell.h"
#import "CVWeekCell.h"
#import "CVDayCell.h"

static NSString * const kCVWeekCellIdentifier = @"CVWeekCell";
static NSString * const kCVDayCellIdentifier = @"CVDayCell";

static int const kNumberOfCalendarColumns = 7;
static int const kNumberOfCalendarRows = 7; // weekday + days

@interface CVMonthCell () {
    UICollectionView *_daysCollectionView;
    NSMutableArray *_cellInfos;
    NSArray *_weekdays;
}

@end

@implementation CVMonthCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _weekdays = @[_(@"周日"), _(@"周一"), _(@"周二"), _(@"周三"), _(@"周四"), _(@"周五"), _(@"周六")];
        
        _selectedDate = [NSDate new];
        self.backgroundColor = [UIColor clearColor];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        _daysCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [self addSubview:_daysCollectionView];
        _daysCollectionView.backgroundColor = [UIColor clearColor];
        _daysCollectionView.delegate = (id)self;
        _daysCollectionView.dataSource = (id)self;
        
        [_daysCollectionView registerClass:[CVWeekCell class] forCellWithReuseIdentifier:kCVWeekCellIdentifier];
        [_daysCollectionView registerClass:[CVDayCell class] forCellWithReuseIdentifier:kCVDayCellIdentifier];
        
        NSDateComponents *components = [kUtil componentsOfDate:[NSDate new]];
        [self reloadDataWithMonthComponents:components];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    _daysCollectionView.frame = self.bounds;
    [_daysCollectionView.collectionViewLayout invalidateLayout];
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return kNumberOfCalendarColumns;
    }
    
    return _cellInfos.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CVWeekCell *weekCell = [_daysCollectionView dequeueReusableCellWithReuseIdentifier:kCVWeekCellIdentifier forIndexPath:indexPath];
        weekCell.weekLabel.text = _weekdays[indexPath.item];
        weekCell.weekLabel.textColor = self.weekTextColor;
        return weekCell;
    }
    
    CVDayCell *dayCell = [_daysCollectionView dequeueReusableCellWithReuseIdentifier:kCVDayCellIdentifier forIndexPath:indexPath];
    dayCell.textColor = _dayTextColor;
    dayCell.fadeoutTextColor = _dayFadeoutTextColor;
    dayCell.highlightedTextColor = _selectedDayTextColor;
    dayCell.highlightedBackgroundColor = _selectedDayBackgroundColor;
    
    id info = _cellInfos[indexPath.item];
    if (info == [NSNull null]) {
        dayCell.state = CVDayCellStateInvisible;
        dayCell.todaySymbol.hidden = YES;
        return dayCell;
    }
    
    NSDateComponents *components = info;
    
    dayCell.textLabel.text = FEStringWithFormat(@"%ld", components.day);
    
    NSDate *date = [kCalendar dateFromComponents:components];
    NSDate *now = [NSDate new];
    
    if (_selectedDate && [kCalendar isDate:date inSameDayAsDate:_selectedDate]) {
        dayCell.state = CVDayCellStateSelected;
    }
    else if (components.month == _monthComponents.month && [kUtil isLaterDate:date]) {
        dayCell.state = CVDayCellStateFadeout;
    }
    else {
        dayCell.state = CVDayCellStateRegular;
    }
    
    // “今天”标志
    dayCell.todaySymbol.hidden = ([kCalendar isDate:date inSameDayAsDate:now] == NO || dayCell.state == CVDayCellStateSelected);
    
    return dayCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(floorf(_daysCollectionView.width / kNumberOfCalendarColumns),
                      floorf(_daysCollectionView.height / kNumberOfCalendarRows));
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return;
    }
    
    id info = _cellInfos[indexPath.item];
    
    if (info == [NSNull null]) {
        return;
    }
    
    NSDateComponents *components = info;
    NSDate *date = [kCalendar dateFromComponents:components];
    
    if ([kUtil isLaterDate:date]) {
        return;
    }
    
    [self setSelectedDate:date];
    
    if ([_delegate respondsToSelector:@selector(monthCell:didSelectDate:)]) {
        [_delegate monthCell:self didSelectDate:date];
    }
}

#pragma mark - Private
- (void)reloadDataWithMonthComponents:(NSDateComponents*)components {
    NSDate *date = [kCalendar dateFromComponents:components];
    NSDateComponents *firstDateComponents = [kCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday fromDate:date];
    firstDateComponents.day = 1;

    _monthComponents = firstDateComponents;
    NSDate *firstDate = [kCalendar dateFromComponents:firstDateComponents];
    
    NSInteger daysOfCurrentMonth = [kUtil daysOfMonthWithDateComponents:firstDateComponents];
    NSInteger startWeekday = [kUtil weekdayOfDate:firstDate];
    
    // 组合
    _cellInfos = [NSMutableArray array];
    for (int i=1; i<startWeekday; i++) {
        [_cellInfos addObject:[NSNull null]];
    }
    
    for (int i=0; i<daysOfCurrentMonth; i++) {
        NSDateComponents *copy = [_monthComponents copy];
        copy.day += i;
        [_cellInfos addObject:copy];
    }
    
    [_daysCollectionView reloadData];
}

#pragma mark - Public
- (void)setSelectedDate:(NSDate *)selectedDate {
    _selectedDate = selectedDate;
    [_daysCollectionView reloadData];
}

- (void)setMonthComponents:(NSDateComponents *)monthComponents {
    [self reloadDataWithMonthComponents:monthComponents];
}

@end
