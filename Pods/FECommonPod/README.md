# FECommonPod

[![CI Status](http://img.shields.io/travis/linchangfeng/FECommonPod.svg?style=flat)](https://travis-ci.org/linchangfeng/FECommonPod)
[![Version](https://img.shields.io/cocoapods/v/FECommonPod.svg?style=flat)](http://cocoapods.org/pods/FECommonPod)
[![License](https://img.shields.io/cocoapods/l/FECommonPod.svg?style=flat)](http://cocoapods.org/pods/FECommonPod)
[![Platform](https://img.shields.io/cocoapods/p/FECommonPod.svg?style=flat)](http://cocoapods.org/pods/FECommonPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FECommonPod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "FECommonPod"
```

## Author

linchangfeng, linchangfeng@live.com

## License

FECommonPod is available under the MIT license. See the LICENSE file for more info.
