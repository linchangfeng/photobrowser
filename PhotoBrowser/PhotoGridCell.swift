//
//  PhotoGridCell.swift
//  PhotoBrowser
//
//  Created by flowdev on 9/5/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class PhotoGridCell: UICollectionViewCell {
    
    var thumbnailImageView: UIImageView!
    var selectedImageView: UIImageView!
    var videoBar: VideoBarView!
    
    var representedAssetIdentifier: String?
    
    var cSelected: Bool = false {
        didSet {
            selectedImageView.hidden = !cSelected
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        thumbnailImageView = UIImageView()
        thumbnailImageView.contentMode = .ScaleAspectFill
        thumbnailImageView.clipsToBounds = true
        
        selectedImageView = UIImageView(image: UIImage(named: "Selected"))
        selectedImageView.contentMode = .ScaleAspectFill
        selectedImageView.clipsToBounds = true
        selectedImageView.hidden = true
        
        videoBar = VideoBarView()
        videoBar.hidden = true
        
        self.contentView.addSubview(thumbnailImageView)
        self.contentView.addSubview(videoBar)
        self.contentView.addSubview(selectedImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        thumbnailImageView.frame = CGRectMake(0, 0, self.width, self.height)
        
        selectedImageView.frame = CGRect(x: 0, y: 0, width: self.width, height: self.height)
        
        videoBar.frame = CGRectMake(0, 0, self.width, self.height / 5)
        videoBar.bottom = self.height
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.videoBar.hidden = true
        self.videoBar.durationLabel.text = ""
        
        self.thumbnailImageView.image = nil
    }
}
