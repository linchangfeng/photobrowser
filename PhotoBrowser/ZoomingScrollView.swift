//
//  ZoomingScrollView.swift
//  PhotoPreview
//
//  Created by flowdev on 9/2/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class ZoomingScrollView: UIScrollView {
    
    var index: Int?
    var token: dispatch_once_t = 0
    
    var image: UIImage? {
        didSet {
            displayImage()
            setNeedsLayout()
        }
    }
    
    override var backgroundColor: UIColor? {
        didSet {
            tapView.backgroundColor = backgroundColor
        }
    }
    
    override var frame: CGRect {
        didSet {
            setMaxMinZoomScalesForCurrentBounds()
        }
    }
    
    private(set) var tapView: TapDetectingView!
    private(set) var photoImageView: TapDetectingImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tapView = TapDetectingView()
        
        photoImageView = TapDetectingImageView()
        photoImageView.contentMode = .ScaleAspectFit
        photoImageView.clipsToBounds = true
        
        delegate = self
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        decelerationRate = UIScrollViewDecelerationRateFast
        
        addSubview(tapView)
        addSubview(photoImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        tapView.frame = self.frame
        
        // Center the image as it becomes smaller than the size of the screen
        var frameToCenter = photoImageView.frame
        
        // Horizontally
        if frameToCenter.width < frame.size.width {
            frameToCenter.origin.x = floor((frame.size.width - frameToCenter.width) / 2.0)
        } else{
            frameToCenter.origin.x = 0.0
        }
        
        // Vertically
        if frameToCenter.height < frame.size.height {
            frameToCenter.origin.y = floor((frame.size.height - frameToCenter.height) / 2.0)
        } else {
            frameToCenter.origin.y = 0.0
        }
        
        photoImageView.frame = frameToCenter
        
    }
    
    func zoomToPoint(zoomPoint: CGPoint, withScale scale: CGFloat, animated: Bool) {
        let contentSize = CGSize(width: self.contentSize.width / self.zoomScale, height: self.contentSize.height / self.zoomScale)
        
        let point = CGPoint(x: (zoomPoint.x / self.bounds.size.width) * contentSize.width, y: (zoomPoint.y / self.bounds.size.height) * contentSize.height)
        
        let zoomSize = CGSize(width: self.bounds.size.width / scale, height: self.bounds.size.height / scale)
        
        let zoomRect = CGRectMake(point.x - zoomSize.width / 2.0, zoomPoint.y - zoomSize.height / 2.0, zoomSize.width, zoomSize.height)
        
        self.zoomToRect(zoomRect, animated: animated)
    }
    
    // MARK: - Private Methods
    private func displayImage() {
        if image != nil {
            maximumZoomScale = 1.0
            minimumZoomScale = 1.0
            zoomScale = 1.0
            contentSize = CGSize.zero
            
            photoImageView.image = image
            
            // Setup photo frame
            photoImageView.frame = CGRect(origin: CGPoint.zero, size: image!.size)
            contentSize = photoImageView.frame.size
            
            // Set zoom to minimum zoom
            setMaxMinZoomScalesForCurrentBounds()
            
            setNeedsLayout()
        }
    }
    
    private func setMaxMinZoomScalesForCurrentBounds() {
        guard image != nil else {
            return
        }
        
        let imageSize = image!.size
        
        // Calculate Min
        // the scale needed to perfectly fit the image width-wise
        let xScale = bounds.width / imageSize.width
        
        // the scale needed to perfectly fit the image height-wise
        let yScale = bounds.height / imageSize.height
        
        // use minimum of these to allow the image to become fully visible
        var minimumScale = min(xScale, yScale)
        
        if xScale == 0 || yScale == 0 {
            minimumScale = 1.0
        }
        
        minimumZoomScale = minimumScale
        
        zoomScale = minimumZoomScale
        
        // Disable scrolling initially until the first pinch to fix issues with swiping on an initally zoomed in photo
        scrollEnabled = false
    }
}

// MARK: - UIScroll View Delegate
extension ZoomingScrollView: UIScrollViewDelegate {
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return photoImageView
    }
    
    func scrollViewWillBeginZooming(scrollView: UIScrollView, withView view: UIView?) {
        scrollEnabled = true
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        setNeedsLayout()
        layoutIfNeeded()
    }
}