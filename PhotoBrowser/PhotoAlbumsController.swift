//
//  PhotoAlbumsController.swift
//  PhotoBrowser
//
//  Created by flowdev on 9/5/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import Photos

class PhotoAlbumsController: UITableViewController {
    
    private let photoGirdThumbnailDimension: CGSize = {
        let scale = UIScreen.mainScreen().scale
        return CGSize(width: 84.0 * scale, height: 84.0 * scale)
    }()
    
    private var assetCollections: [PHAssetCollection] = []
    private var assetsFetchResults: [PHFetchResult] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Ablums"
        
        tableView.tableFooterView = UIView()
        
        initialPhotos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard segue.destinationViewController is PhotoPickerController || sender is UITableViewCell else {
            return
        }
        
        let photoPickerController = segue.destinationViewController as! PhotoPickerController
        
        if let cell = sender as? PhotoAlbumListCell {
            if let indexPath = tableView.indexPathForCell(cell) {
                photoPickerController.currentassetCollection = assetCollections[indexPath.row]
                photoPickerController.assetsFetchResult = assetsFetchResults[indexPath.row]
            }
        }
    }
}

// MARK: - Table view Data Source
extension PhotoAlbumsController {
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return assetCollections.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PhotoAlbumListCell", forIndexPath: indexPath) as! PhotoAlbumListCell
        
        if indexPath.row >= assetsFetchResults.count {
            
            let options = PHFetchOptions()
            options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            
            assetsFetchResults.append(PHAsset.fetchAssetsInAssetCollection(assetCollections[indexPath.row], options: options))
        }
        
        let assetFetchResult = assetsFetchResults[indexPath.row]
        
        if assetFetchResult.count > 0 {
            let asset = assetFetchResult.firstObject as! PHAsset
            
            PHImageManager.defaultManager().requestImageForAsset(asset, targetSize: photoGirdThumbnailDimension, contentMode: .AspectFill, options: nil) {
                (image: UIImage?, info: [NSObject: AnyObject]?) in
                
                if let image = image {
                    cell.photoAlbumThumbnailImageView.image = image
                }
            }
        }
        
        cell.photoAlbumTitleLabel.text = assetCollections[indexPath.row].localizedTitle
        
        cell.photoAlbumCountLabel.text = String(assetsFetchResults[indexPath.row].count)
        
        return cell
    }
}

// MARK: - Table view delegate
extension PhotoAlbumsController {
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}

// MARK: - Button Action
extension PhotoAlbumsController {
    @IBAction func dismiss(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

// MARK: - Private
extension PhotoAlbumsController {
    func initialPhotos() {
        
        // Fetch the canera roll album, recetly added ablum, regular albums
        let cameraRollAlbum = PHAssetCollection.fetchAssetCollectionsWithType(.SmartAlbum, subtype: .SmartAlbumUserLibrary, options: nil)
        if let cameraRollAlbum = cameraRollAlbum.firstObject as? PHAssetCollection {
            assetCollections.append(cameraRollAlbum)
        }
        
        let recentlyAddeAlbum = PHAssetCollection.fetchAssetCollectionsWithType(.SmartAlbum, subtype: .SmartAlbumRecentlyAdded, options: nil)
        if let recentlyAddeAlbum = recentlyAddeAlbum.firstObject as? PHAssetCollection {
            assetCollections.append(recentlyAddeAlbum)
        }
        
        let regularAlbums = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .AlbumRegular, options: nil)
        regularAlbums.enumerateObjectsUsingBlock { (regularAlbum, index, _) in
            if let regularAlbum = regularAlbum as? PHAssetCollection {
                self.assetCollections.append(regularAlbum)
            }
        }
        
        // Fetch the screenshots album if the system version of the device is greater than iOS9
        if #available(iOS 9.0, *) {
            let screenshotsAlbum = PHAssetCollection.fetchAssetCollectionsWithType(.SmartAlbum, subtype: .SmartAlbumScreenshots, options: nil)
            if let screenshotsAlbum = screenshotsAlbum.firstObject as? PHAssetCollection {
                assetCollections.append(screenshotsAlbum)
            }
        }
    }
}


