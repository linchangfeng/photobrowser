//
//  VideoBarView.swift
//  PhotoBrowser
//
//  Created by flowdev on 9/6/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import FECommonPod

class VideoBarView: UIView {
    
    var imageView = UIImageView(image: UIImage(named: " Video"))
    var durationLabel = UILabel()
    
    init() {
        super.init(frame: CGRect.zero)
        
        self.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.8)
        
        self.imageView.contentMode = .ScaleAspectFit
        self.imageView.clipsToBounds = true
        
        self.durationLabel.textColor = UIColor.whiteColor()
        self.durationLabel.font = UIFont.systemFontOfSize(10)
        self.durationLabel.textAlignment = .Right
        
        self.addSubview(self.imageView)
        self.addSubview(durationLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.imageView.centerY = self.halfHeight
        
        self.durationLabel.frame = CGRectMake(self.imageView.right, 0, self.width - self.imageView.width, self.height)
        self.durationLabel.centerY = self.halfHeight
    }
    
}
