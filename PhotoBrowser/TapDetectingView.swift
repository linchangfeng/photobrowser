//
//  TapDetectingView.swift
//  PhotoPreview
//
//  Created by flowdev on 9/2/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

@objc protocol TapDetectingViewDelegate {
    
    optional func singleTapDetectedFromTapView(touch: UITouch)
    optional func doubleTapDetectedFromTapView(touch: UITouch)
}

class TapDetectingView: UIView {
    
    weak var delegate: TapDetectingViewDelegate?
    
    // MARK: - Initialization
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        let touch = (touches as NSSet).anyObject() as! UITouch
        
        switch touch.tapCount {
        case 1:
            performSelector(#selector(TapDetectingView.handleSingleTap), withObject: touch, afterDelay: 0.3)
        case 2:
            NSObject.cancelPreviousPerformRequestsWithTarget(self)
            handleDoubleTap(touch)
        default:
            break

        }
    }
    
    @objc private func handleSingleTap(touch: UITouch) {
        delegate?.singleTapDetectedFromTapView?(touch)
    }
    
    @objc private func handleDoubleTap(touch: UITouch) {
        delegate?.doubleTapDetectedFromTapView?(touch)
    }
}