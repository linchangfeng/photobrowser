//
//  TapDetectingImageView.swift
//  PhotoPreview
//
//  Created by flowdev on 9/2/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

@objc protocol TapDetectingImageViewDelegate {
    
    optional func singleTapDetected(touch: UITouch)
    optional func doubleTapDetected(touch: UITouch)
}

class TapDetectingImageView: UIImageView {
    
    weak var delegate: TapDetectingImageViewDelegate?
    
    convenience init() {
        self.init(frame: CGRectZero)
        
        self.userInteractionEnabled = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.userInteractionEnabled = true
    }
    
    override init(image: UIImage?) {
        super.init(image: image)
        
        self.userInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(image: UIImage?, highlightedImage: UIImage?) {
        super.init(image: image, highlightedImage: highlightedImage)
        
        self.userInteractionEnabled = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    // MARK: - Overridden Methods
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        let touch = (touches as NSSet).anyObject() as! UITouch
        
        switch touch.tapCount {
        case 1:
            performSelector(#selector(TapDetectingImageView.handleSingleTap), withObject: touch, afterDelay: 0.3)
        case 2:
            NSObject.cancelPreviousPerformRequestsWithTarget(self)
            handleDoubleTap(touch)
        default:
            break
        }
    }
    
    // MARK: - Private Methods
    @objc private func handleSingleTap(touch: UITouch) {
        delegate?.singleTapDetected?(touch)
    }
    
    @objc private func handleDoubleTap(touch: UITouch) {
        delegate?.doubleTapDetected?(touch)
    }
}
