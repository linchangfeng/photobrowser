//
//  PhotoPickerController.swift
//  PhotoBrowser
//
//  Created by flowdev on 9/5/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import Photos
import FECommonPod

enum SortWithCreateDate {
    case Ascending
    case Descending
}

struct Selectable {
    var selected: Bool = false
    var asset: PHAsset = PHAsset()
}

private let reuseIdentifier = "Cell"

class PhotoPickerController: UICollectionViewController {
    
    @IBOutlet weak var selectableSegmented: UISegmentedControl!
    @IBOutlet var toolbarview: UIView!
    
    var currentassetCollection: PHAssetCollection = PHAssetCollection()
    
    var assetsFetchResult: PHFetchResult = PHFetchResult() {
        didSet {
            assets = []
            assetsFetchResult.enumerateObjectsUsingBlock {
                asset, index, _ in
                
                var selectable = Selectable()
                selectable.asset = asset as! PHAsset
                self.assets.append(selectable)
            }
        }
    }
    var assets: [Selectable] = []
    
    private lazy var photoGirdThubmnailDimension: CGSize = {
        let scale = UIScreen.mainScreen().scale
        let cellSize = (self.collectionViewLayout as! UICollectionViewFlowLayout).itemSize
        
        return CGSize(width: cellSize.width * scale, height: cellSize.height * scale)
    }()
    
    private let toolbarButtonItemDimension = CGSize(width: 40.0, height: 20.0)
    private let toolbarViewHeight: CGFloat = 44.0
    
    private var toolbarButtonItem: UIBarButtonItem!
    private var fixedSpaceBarButtonItem: UIBarButtonItem!
    
    private let  padding: CGFloat = 8.0
    private let imageManager = PHCachingImageManager()
    private var photoPickerControllerFirstShowUp = true
    private var finishedUpdatingCachedAssets: Bool = false
    private var previousPreheatRect = CGRect.zero
    private var photos: [PHAsset] = []
    private var currentSortType: SortWithCreateDate = .Descending
    private var allSelected = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "All Photos"
        
        fixedSpaceBarButtonItem = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
        
        toolbarButtonItem = UIBarButtonItem(customView: toolbarview)
        toolbarItems = [fixedSpaceBarButtonItem, toolbarButtonItem]
        
        selectableSegmented.addTarget(self, action: #selector(PhotoPickerController.segmentedSelectionChanged(_:)), forControlEvents: .ValueChanged)
        
        collectionView!.allowsMultipleSelection = true
        
        collectionView?.registerClass(PhotoGridCell.self, forCellWithReuseIdentifier: "PhotoGirdCell")
        
        if #available(iOS 9.0, *) {
            if traitCollection.forceTouchCapability == .Available {
                self.registerForPreviewingWithDelegate(self, sourceView: collectionView!)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        setupCollectionLayout()
        
        toolbarview.frame = CGRect(x: 0, y: 0, width: view.width, height: (self.navigationController?.toolbar.frame.size.height)!)
        fixedSpaceBarButtonItem.width = -(padding * 2.0)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if photoPickerControllerFirstShowUp {
            photoPickerControllerFirstShowUp = false
            
            let updateOperation = NSBlockOperation(block: updateCachedAssets)
            updateOperation.completionBlock = {
                self.finishedUpdatingCachedAssets = true
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.collectionView!.reloadData()
                }
            }
            
            let operationQueue = NSOperationQueue()
            operationQueue.addOperation(updateOperation)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if photos.count == 0 {
            assetsFetchResult.enumerateObjectsUsingBlock({ (asset, index, _) in
                self.photos.append(asset as! PHAsset)
            })
        }
    }
}

// MARK: - UICollection View Data Source
extension PhotoPickerController {
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return finishedUpdatingCachedAssets ? assets.count : 0
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PhotoGirdCell", forIndexPath: indexPath) as! PhotoGridCell
        
        let selectable = assets[indexPath.row]
        
        cell.cSelected = selectable.selected
        
        let asset = selectable.asset
        
        if asset.mediaType == .Video {
            cell.videoBar.hidden = false
            cell.videoBar.durationLabel.text = FEUtil.sharedInstance().formatDuration(Int(round(asset.duration)))
        }
        
        cell.representedAssetIdentifier = asset.localIdentifier

        let options = PHImageRequestOptions()
        options.deliveryMode = .Opportunistic
        options.resizeMode = .Fast
        
        imageManager.requestImageForAsset(asset, targetSize: photoGirdThubmnailDimension, contentMode: .AspectFill, options: options) {
            (image: UIImage?, info: [NSObject : AnyObject]?) in
            
            // Set the cell's thumbnail image if it's still showing the same asset
            if cell.representedAssetIdentifier == asset.localIdentifier {
                cell.thumbnailImageView.image = image
            }
        }
        
        return cell
    }
}

// MARK: - UICollection View Delegate
extension PhotoPickerController {
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        var selectable = assets[indexPath.row]
        selectable.selected = !selectable.selected
        assets[indexPath.row] = selectable
        collectionView.reloadItemsAtIndexPaths([indexPath])
    }
}

// MARK: - UICollection View Flow Layout Delegate
extension PhotoPickerController: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let minorPadding = padding / 2.0
        
        return UIEdgeInsets(top: minorPadding, left: minorPadding, bottom: minorPadding, right: minorPadding)
    }
}

// MARK: - Scroll View Delegate
extension PhotoPickerController {
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        // Update cached assets for the new visible area.
        updateCachedAssets()
    }
}

// MARK: - Button action
extension PhotoPickerController {
    @IBAction func sortAction() {
        currentSortType = currentSortType == .Descending ? .Ascending : .Descending
        sortAssetsWithType(currentSortType)
    }
    
    @IBAction func allSeletAction() {
        allSelected = !allSelected
        for i in assets.startIndex..<assets.endIndex {
            var selectable = assets[i]
            selectable.selected = allSelected
            assets[i] = selectable
        }
        
        refreshCollectionView()
    }
    
    @objc func segmentedSelectionChanged(sender: UISegmentedControl) {
        sortAssetsWithType(currentSortType)
    }
    @IBAction func export(sender: UIBarButtonItem) {
        let tmp = assets.filter { return $0.selected }.map { return $0.asset }
        print(tmp)
    }
}

// MARK: - Private
extension PhotoPickerController {
    
    func setupCollectionLayout() {
        let flowLayout = collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        let minorPadding = padding / 2.0
        var itemWidth: CGFloat = (view.width - minorPadding * 5.0) / 4
        
        if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
            itemWidth = (view.width - minorPadding * 8.0) / 7
        }
        
        flowLayout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        flowLayout.minimumLineSpacing = minorPadding
        flowLayout.minimumInteritemSpacing = minorPadding
    }
    
    func sortAssetsWithType(type: SortWithCreateDate) {

        let options = PHFetchOptions()
        let index = selectableSegmented.selectedSegmentIndex
        
        if type == .Ascending {
            options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            
        } else if type == .Descending {
            options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        }
        
        if index == 1 {
            let predicate = NSPredicate(format: "mediaType = %@", NSNumber(integer: PHAssetMediaType.Image.rawValue))
            options.predicate = predicate
        } else if index == 2 {
            let predicate = NSPredicate(format: "mediaType = %@", NSNumber(integer: PHAssetMediaType.Video.rawValue))
            options.predicate = predicate
        }
        
        assetsFetchResult = PHAsset.fetchAssetsInAssetCollection(currentassetCollection, options: options)
        
        refreshCollectionView()
    }
    
    private func computeDifferenceBetweenRect(oldRect: CGRect, andRect newRect: CGRect, removedHandler: (removedRect: CGRect) -> Void, addedHandler: (addedRect: CGRect) -> Void) {
        let oldMinY = oldRect.minY
        let oldMaxY = oldRect.maxY
        let newMinY = newRect.minY
        let newMaxY = newRect.maxY
        
        if newRect.intersects(oldRect) {
            if newMaxY > oldMaxY {
                let rectToAdd = CGRect(
                    x: newRect.minX,
                    y: oldMaxY,
                    width: newRect.width,
                    height: newMaxY - oldMaxY
                )
                addedHandler(addedRect: rectToAdd)
            }
            
            if oldMinY > newMinY {
                let rectToAdd = CGRect(
                    x: newRect.minX,
                    y: newMinY,
                    width: newRect.width,
                    height: oldMinY - newMinY
                )
                addedHandler(addedRect: rectToAdd)
            }
            
            if newMaxY < oldMaxY {
                let rectToRemove = CGRect(
                    x: newRect.minX,
                    y: newMaxY,
                    width: newRect.width,
                    height: oldMaxY - newMaxY
                )
                removedHandler(removedRect: rectToRemove)
            }
            
            if oldMinY < newMinY {
                let rectToRemove = CGRect(
                    x: newRect.minX,
                    y: oldMinY,
                    width: newRect.width,
                    height: newMinY - oldMinY
                )
                removedHandler(removedRect: rectToRemove)
            }
        } else {
            addedHandler(addedRect: newRect)
            removedHandler(removedRect: oldRect)
        }
    }
    
    private func updateCachedAssets() {
        guard isViewLoaded() && view.window != nil else {
            return
        }
        
        // The preheat window is twice the height of the visible rect
        var preheatRect = collectionView!.bounds
        preheatRect = CGRectInset(preheatRect, 0.0, -0.5 * CGRectGetHeight(preheatRect))
        
        /*
         Check if the collection view is showing an area that is significantly
         different to the last preheated area
         */
        let delta = abs(CGRectGetMidY(preheatRect) - CGRectGetMidY(previousPreheatRect))
        
        if delta > collectionView!.bounds.size.height / 3.0 {
            // Compute the assets to start caching and to stop caching
            var addedIndexPaths: [NSIndexPath] = []
            var removedIndexPaths: [NSIndexPath] = []
            
            computeDifferenceBetweenRect(previousPreheatRect, andRect: preheatRect, removedHandler: {
                removedRect in
                
                let indexPaths = self.collectionView!.indexPathsForElementsInRect(removedRect)
                
                if indexPaths != nil {
                    removedIndexPaths.appendContentsOf(indexPaths!)
                }
                }, addedHandler: {
                    addedRect in
                    
                    let indexPaths = self.collectionView!.indexPathsForElementsInRect(addedRect)
                    
                    if indexPaths != nil {
                        addedIndexPaths.appendContentsOf(indexPaths!)
                    }
            })
            
            let assetsToStartCaching = assetsAtIndexPaths(addedIndexPaths)
            let assetsToStopCaching = assetsAtIndexPaths(removedIndexPaths)
            
            // Update the assets the PHCachingImageManager is caching
            let options: PHImageRequestOptions = {
                let options = PHImageRequestOptions()
                options.deliveryMode = .Opportunistic
                options.resizeMode = .Fast
                
                return options
            }()
            
            if assetsToStartCaching != nil {
                imageManager.startCachingImagesForAssets(assetsToStartCaching!,
                                                         targetSize: photoGirdThubmnailDimension,
                                                         contentMode: .AspectFill,
                                                         options: options)
            }
            
            if assetsToStopCaching != nil {
                imageManager.stopCachingImagesForAssets(assetsToStopCaching!,
                                                        targetSize: photoGirdThubmnailDimension,
                                                        contentMode: .AspectFill,
                                                        options: nil)
            }
            
            // Store the preheat rect to compare against in the future
            previousPreheatRect = preheatRect
        }
    }
    
    private func assetsAtIndexPaths(indexPaths: [NSIndexPath]) -> [PHAsset]? {
        if indexPaths.count == 0 {
            return nil
        }
        
        return indexPaths.map {
            (self.assetsFetchResult[$0.item] as! PHAsset)
        }
    }
    
    private func refreshCollectionView() {
        collectionView?.reloadData()
        if assetsFetchResult.count != 0 {
            collectionView?.scrollToItemAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: .Top, animated: false)
        }
        updateCachedAssets()
    }
}

extension PhotoPickerController: UIViewControllerPreviewingDelegate {
    func previewingContext(previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        guard let indexPath = collectionView?.indexPathForItemAtPoint(location) else { return nil }
        
        guard let cell = collectionView?.cellForItemAtIndexPath(indexPath) as? PhotoGridCell else { return nil }
        
        let photoPreview = PhotoPreviewViewController()
        photoPreview.currentPhotoIndex = indexPath.row
        photoPreview.assets = assets.map { return $0.asset}
        
        let asset = assets[indexPath.row].asset
        let imageAspectRatio = CGFloat(asset.pixelWidth) / CGFloat(asset.pixelHeight)
        var targetSize = CGSize()
        
        if view.width / view.height > imageAspectRatio {
            targetSize.width = view.height * imageAspectRatio
            targetSize.height = view.height
        } else {
            targetSize.width = view.width
            targetSize.height = view.width / imageAspectRatio
        }
        
        photoPreview.preferredContentSize = targetSize
        
        if #available(iOS 9.0, *) {
            previewingContext.sourceRect = cell.frame
        }
        
        return photoPreview
    }
    
    func previewingContext(previewingContext: UIViewControllerPreviewing, commitViewController viewControllerToCommit: UIViewController) {
        showViewController(viewControllerToCommit, sender: self)
    }
}

extension UICollectionView {
    func indexPathsForElementsInRect(rect: CGRect) -> [NSIndexPath]? {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElementsInRect(rect)
        
        return allLayoutAttributes == nil ? nil : allLayoutAttributes!.map { $0.indexPath }
    }
}
