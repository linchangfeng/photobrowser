//
//  AppDelegate.swift
//  PhotoBrowser
//
//  Created by flowdev on 9/5/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import AdSupport

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let appKey = "9ca285f53d272b4e1282922d"
    let channel = "Publish channel"
    let isProduction = false
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        application.applicationSupportsShakeToEdit = true
        
        // regist local notification
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        
        // Operator while app was kill and back to app
        if let options = launchOptions {
            if let _ = options["UIApplicationLaunchOptionsLocalNotificationKey"] {
                application.applicationIconBadgeNumber = 0
                // Do something
            }
            
            if let _ = options["UIApplicationLaunchOptionsRemoteNotificationKey"] {
                application.applicationIconBadgeNumber = 0
            }
        }
        
        let advertisingId = ASIdentifierManager.sharedManager().advertisingIdentifier.UUIDString
        
        JPUSHService.registerForRemoteNotificationTypes(UIUserNotificationType.Badge.rawValue | UIUserNotificationType.Alert.rawValue | UIUserNotificationType.Sound.rawValue, categories: nil)
        
        JPUSHService.setupWithOption(launchOptions, appKey: appKey, channel: channel, apsForProduction: isProduction, advertisingIdentifier: advertisingId)
        
        addJPUSHServiceNotification()
        
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        
    }
    
    func applicationWillTerminate(application: UIApplication) {
        
    }
    
    @available(iOS 9.0, *)
    func application(application: UIApplication, performActionForShortcutItem shortcutItem: UIApplicationShortcutItem, completionHandler: (Bool) -> Void) {
        print(shortcutItem.localizedTitle)
    }
    
    // When app is running in foreground
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        let state = application.applicationState
        
        if state == .Active {
            
            let alert = UIAlertController(title: "", message: notification.alertBody, preferredStyle: .Alert)
            
            let ok = UIAlertAction(title: "OK", style: .Default, handler: nil)
            
            alert.addAction(ok)
            
            window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            
            application.applicationIconBadgeNumber = 0
        }
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        JPUSHService.registerDeviceToken(deviceToken)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        JPUSHService.handleRemoteNotification(userInfo)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        JPUSHService.handleRemoteNotification(userInfo)
        print("AAAA=========:::\(userInfo)")
        completionHandler(.NewData)
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Did fail to register for remote notifications with error: \(error)")
    }
}

extension AppDelegate {
    func addJPUSHServiceNotification() {
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(AppDelegate.handleJPFNetworkDidSetupNotification),
                                                         name: kJPFNetworkDidSetupNotification,
                                                         object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(AppDelegate.handleJPFNetworkDidCloseNotification),
                                                         name: kJPFNetworkDidCloseNotification,
                                                         object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(AppDelegate.handleJPFNetworkDidRegisterNotification),
                                                         name: kJPFNetworkDidRegisterNotification,
                                                         object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(AppDelegate.handleJPFNetworkDidLoginNotification),
                                                         name: kJPFNetworkDidLoginNotification,
                                                         object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(AppDelegate.handleJPFNetworkDidReceiveMessageNotification),
                                                         name: kJPFNetworkDidReceiveMessageNotification,
                                                         object: nil)
    }
    
    func remoteJPUSHServiceNotification() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kJPFNetworkDidSetupNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kJPFNetworkDidCloseNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kJPFNetworkDidRegisterNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kJPFNetworkDidLoginNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kJPFNetworkDidReceiveMessageNotification, object: nil)
    }
    
    @objc func handleJPFNetworkDidSetupNotification(notification: NSNotification) {
        
    }
    
    @objc func handleJPFNetworkDidCloseNotification(notification: NSNotification) {
        
    }
    
    @objc func handleJPFNetworkDidRegisterNotification(notification: NSNotification) {
        
    }
    
    @objc func handleJPFNetworkDidLoginNotification(notification: NSNotification) {
        
    }
    
    // just use for custom message and app is running in foreground
    @objc func handleJPFNetworkDidReceiveMessageNotification(notification: NSNotification) {
        print("++++: \(notification.userInfo)")
    }
}

