//
//  ViewController.swift
//  PhotoBrowser
//
//  Created by flowdev on 9/5/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import Photos

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        addLocalNotification()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
        UIDevice.currentDevice().proximityMonitoringEnabled = true
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(ViewController.handleDeviceOrientationDidChange),
                                                         name: UIDeviceOrientationDidChangeNotification,
                                                         object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(ViewController.handleDeviceProximityStateDidChange),
                                                         name: UIDeviceProximityStateDidChangeNotification,
                                                         object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.resignFirstResponder()
        
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        UIDevice.currentDevice().endGeneratingDeviceOrientationNotifications()
        UIDevice.currentDevice().proximityMonitoringEnabled = false
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIDeviceOrientationDidChangeNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIDeviceProximityStateDidChangeNotification, object: nil)
    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        if motion == .MotionShake {
            print("shake")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func canBecomeFirstResponder() -> Bool {
        super.canBecomeFirstResponder()
        
        return true
    }
    
    @IBAction func picketPhotos(sender: AnyObject) {
        
        let authorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        if authorizationStatus == .Authorized {
            
            let photoAlbumController = storyboard?.instantiateViewControllerWithIdentifier("PhotoAlbumListController") as! UINavigationController
            presentViewController(photoAlbumController, animated: true, completion: nil)
            
        } else if authorizationStatus == .NotDetermined {
            
            PHPhotoLibrary.requestAuthorization {
                authorizationStatus in
                
                dispatch_async(dispatch_get_main_queue(), { 
                    if authorizationStatus == .Authorized {
                        let photoAlbumController = self.storyboard?.instantiateViewControllerWithIdentifier("PhotoAlbumListController") as! UINavigationController
                        self.presentViewController(photoAlbumController, animated: true, completion: nil)

                    } else {
                        self.showAuthorizedAlert()
                    }
                })
            }
            
        } else {
            showAuthorizedAlert()
        }
    }
    
    func showAuthorizedAlert() {
        let alert = UIAlertController(title: "", message: "Please go to setting and enable PhotoBrowser to accest your Camera Roll", preferredStyle: .Alert)
        let ok = UIAlertAction(title: "Sure", style: .Default, handler: nil)
        alert.addAction(ok)
        
        presentViewController(alert, animated: true, completion: nil)
    }
}

// MARK: - Mixed
extension ViewController {
    @objc func handleDeviceOrientationDidChange() {
        let device = UIDevice.currentDevice()
        
        switch device.orientation {
        case .FaceUp:
            print("FaceUp")
        case .FaceDown:
            print("FaceDown")
        case .LandscapeLeft:
            print("LandscapeLeft")
        case .LandscapeRight:
            print("LandscapeRight")
        case .Portrait:
            print("Portrait")
        case .PortraitUpsideDown:
            print("PortraitUpsideDown")
        case .Unknown:
            print("Unknown")
        }
    }
    
    @objc func handleDeviceProximityStateDidChange() {
        let device = UIDevice.currentDevice()
        
        if device.proximityState {  // Black screen
            print("proximity state: true")
        } else {
            print("proximity state: false")
        }
    }
    
    @objc func addLocalNotification() {
        let localNotification = UILocalNotification()
        
        // notification  fire time
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 5.0)
        
        // alert content
        localNotification.alertBody = "Coding..."
        
        // Lock screen notification
        localNotification.hasAction = true
        localNotification.alertAction = "Lock screen local notification"
        
        // notification title
        if #available(iOS 8.2, *) {
            localNotification.alertTitle = "Local notification"
        }
        
        localNotification.applicationIconBadgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber + 1
        
        // localNotification.soundName = "" // Default is system notification sound, or set to seach in main bundle
        
        // add notification
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }
}

