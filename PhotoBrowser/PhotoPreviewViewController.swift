//
//  PhotoPreviewViewController.swift
//  PhotoPreview
//
//  Created by flowdev on 9/2/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import Photos
import MediaPlayer
import AVKit

class PhotoPreviewViewController: UIViewController {
    
    let padding: CGFloat = 10.0
    
    var currentPhotoIndex: Int!
    var assets: [PHAsset]!
    
    private var pageControl: UIPageControl!
    private var collectionView = UICollectionView(
        frame: CGRect.zero,
        collectionViewLayout: UICollectionViewFlowLayout()
    )
    
    private var navigationBarHidden = false
    
    private var defaultBackgoundColor = UIColor.whiteColor() {
        didSet {
            collectionView.backgroundColor = defaultBackgoundColor
            
            let cell = collectionView.cellForItemAtIndexPath(NSIndexPath(forRow: currentPhotoIndex, inSection: 0)) as! PhotoPreviewCollectionViewCell
            cell.backgroundColor = defaultBackgoundColor
        }
    }
    
    private lazy var targetSize: CGSize = {
       return CGSize(
            width: UIScreen.mainScreen().bounds.width * UIScreen.mainScreen().scale,
            height: UIScreen.mainScreen().bounds.height * UIScreen.mainScreen().scale
        )
    }()
    
    private let imageManager = PHCachingImageManager()
    private var photoPickerControllerFirstShowUp = true
    private var finishedUpdatingCachedAssets: Bool = false
    private var previousPreheatRect = CGRect.zero
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.hidesBottomBarWhenPushed = true
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.pagingEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceHorizontal = true
        collectionView.backgroundColor = UIColor.whiteColor()
        
        collectionView.registerClass(PhotoPreviewCollectionViewCell.self, forCellWithReuseIdentifier: "PreviewCell")
        
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        flowLayout.scrollDirection = .Horizontal
        flowLayout.minimumLineSpacing = 0.0
        
        self.view.addSubview(collectionView)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        collectionView.frame = CGRectMake(0, 0, self.view.frame.size.width + padding * 2, self.view.frame.size.height)
        
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        flowLayout.itemSize = CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        
        collectionView.scrollToItemAtIndexPath(NSIndexPath(forRow: currentPhotoIndex, inSection: 0), atScrollPosition: .None, animated: false)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Begin caching assets in and around collection view's visible rect
        updateCachedAssets()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return navigationBarHidden
    }
}

extension PhotoPreviewViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assets.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PreviewCell", forIndexPath: indexPath) as! PhotoPreviewCollectionViewCell
        
        let asset = assets[indexPath.row]
        cell.representedAssetIdentifier = asset.localIdentifier
        
        cell.page.tapView.delegate = self
        cell.page.photoImageView.delegate = self
        cell.backgroundColor = defaultBackgoundColor
        
        // Request an image for the asset from the PHCachingImageManager
        let options = PHImageRequestOptions()
        options.deliveryMode = .HighQualityFormat
        options.resizeMode = .Fast
        options.synchronous = false
        
        imageManager.requestImageForAsset(asset, targetSize: targetSize, contentMode: .AspectFill, options: options) {
            (image: UIImage?, info: [NSObject : AnyObject]?) in
            
            // Set the cell's thumbnail image if it's still showing the same asset
            if cell.representedAssetIdentifier == asset.localIdentifier {
                cell.page.image = image
            }
        }
        
        if asset.mediaType == .Video {
            cell.playButton.hidden = false
            cell.playButton.addTarget(self, action: #selector(PhotoPreviewViewController.playVideo), forControlEvents: .TouchUpInside)
        }
        
        return cell
    }
}

// MARK: - Button action
extension PhotoPreviewViewController {
    @objc func playVideo() {
        let asset = assets[currentPhotoIndex]
        if asset.mediaType == .Video {
            
            navigationBarHidden = true
            hideNavigationBarStatusBar(navigationBarHidden)
            
            imageManager.requestAVAssetForVideo(asset, options: nil, resultHandler: { (avAsset, audioMix, info) in
                dispatch_async(dispatch_get_main_queue(), {
                    let url = (avAsset as! AVURLAsset).URL
//                    let player = MPMoviePlayerViewController(contentURL: url)
//                    player.moviePlayer.prepareToPlay()
//                    player.moviePlayer.shouldAutoplay = true
//                    self.presentViewController(player, animated: true, completion: nil)
                    
                    let player = AVPlayer(URL: url)
                    let playerController = AVPlayerViewController()
                    playerController.player = player
                    self.addChildViewController(playerController)
                    self.view.addSubview(playerController.view)
                    playerController.view.frame = self.view.frame
                    
                    player.play()
                })
            })
        }
    }
}

// MARK: - Scroll view delegate
extension PhotoPreviewViewController {
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let offest = scrollView.contentOffset
        currentPhotoIndex = Int(offest.x / collectionView.frame.size.width)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        updateCachedAssets()
    }
}

extension PhotoPreviewViewController: TapDetectingViewDelegate {
    func singleTapDetected(touch: UITouch) {
        
        defaultBackgoundColor = (defaultBackgoundColor == UIColor.blackColor()) ? UIColor.whiteColor() : UIColor.blackColor()
        
        if navigationController != nil {
            
            navigationBarHidden = !navigationBarHidden
            
            hideNavigationBarStatusBar(navigationBarHidden)
        }
    }
    
    func doubleTapDetected(touch: UITouch) {
        
        navigationBarHidden = true
        hideNavigationBarStatusBar(true)
        
        defaultBackgoundColor = UIColor.blackColor()
        
        let cell = collectionView.cellForItemAtIndexPath(NSIndexPath(forRow: currentPhotoIndex, inSection: 0)) as! PhotoPreviewCollectionViewCell
        
        if cell.page.zoomScale == cell.page.minimumZoomScale {
            cell.page.zoomToPoint(touch.locationInView(cell.page), withScale: cell.page.maximumZoomScale, animated: true)
        } else {
            cell.page.zoomToPoint(touch.locationInView(cell.page), withScale: cell.page.minimumZoomScale, animated: true)
        }
    }
}

extension PhotoPreviewViewController: TapDetectingImageViewDelegate {
    func singleTapDetectedFromTapView(touch: UITouch) {
        singleTapDetected(touch)
    }
    
    func doubleTapDetectedFromTapView(touch: UITouch) {
        
    }
}

// MARK: - Private
extension PhotoPreviewViewController {
    func hideNavigationBarStatusBar(hide: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        
        UIApplication.sharedApplication().setStatusBarHidden(hide, withAnimation: .Fade)
        self.navigationController!.setNavigationBarHidden(hide, animated: true)
    }
    
    private func assetsAtIndexPaths(indexPaths: [NSIndexPath]) -> [PHAsset]? {
        if indexPaths.count == 0 {
            return nil
        }
        
        return indexPaths.map {
            self.assets[$0.item]
        }
    }
    
    private func computeDifferenceBetweenRect(oldRect: CGRect, andRect newRect: CGRect, removedHandler: (removedRect: CGRect) -> Void, addedHandler: (addedRect: CGRect) -> Void) {
        let oldMinX = oldRect.minX
        let oldMaxX = oldRect.maxX
        let newMinX = newRect.minX
        let newMaxX = newRect.maxX
        
        if newRect.intersects(oldRect) {
            if newMaxX > oldMaxX {
                let rectToAdd = CGRect(
                    x: oldMaxX,
                    y: newRect.minY,
                    width: newMaxX - oldMaxX,
                    height: newRect.height
                )
                addedHandler(addedRect: rectToAdd)
            }
            
            if oldMinX > newMinX {
                let rectToAdd = CGRect(
                    x: newMinX,
                    y: newRect.minY,
                    width: oldMinX - newMinX,
                    height: newRect.height
                )
                addedHandler(addedRect: rectToAdd)
            }
            
            if newMaxX < oldMaxX {
                let rectToRemove = CGRect(
                    x: newMaxX,
                    y: newRect.minY,
                    width: oldMaxX - newMaxX,
                    height: newRect.height
                )
                removedHandler(removedRect: rectToRemove)
            }
            
            if oldMinX < newMinX {
                let rectToRemove = CGRect(
                    x: oldMinX,
                    y: newRect.minY,
                    width: newMinX - oldMinX,
                    height: newRect.height
                )
                removedHandler(removedRect: rectToRemove)
            }
        } else {
            addedHandler(addedRect: newRect)
            removedHandler(removedRect: oldRect)
        }
    }
    
    private func updateCachedAssets() {
        guard isViewLoaded() && view.window != nil else {
            return
        }
        
        // The preheat window is twice the width of the visible rect
        var preheatRect = collectionView.bounds
        preheatRect = CGRectInset(preheatRect, -preheatRect.width, 0.0)
        
        /*
         Check if the collection view is showing an area that is significantly
         different to the last preheated area
         */
        let delta = abs(CGRectGetMidX(preheatRect) - CGRectGetMidX(previousPreheatRect))
        
        if delta >= collectionView.bounds.size.width / 2.0 {
            // Compute the assets to start caching and to stop caching
            var addedIndexPaths: [NSIndexPath] = []
            var removedIndexPaths: [NSIndexPath] = []
            
            computeDifferenceBetweenRect(previousPreheatRect, andRect: preheatRect, removedHandler: {
                removedRect in
                
                let indexPaths = self.collectionView.indexPathsForElementsInRect(removedRect)
                
                if indexPaths != nil {
                    removedIndexPaths.appendContentsOf(indexPaths!)
                }
                }, addedHandler: {
                    addedRect in
                    
                    let indexPaths = self.collectionView.indexPathsForElementsInRect(addedRect)
                    
                    if indexPaths != nil {
                        addedIndexPaths.appendContentsOf(indexPaths!)
                    }
            })
            
            let assetsToStartCaching = assetsAtIndexPaths(addedIndexPaths)
            let assetsToStopCaching = assetsAtIndexPaths(removedIndexPaths)
            
            // Update the assets the PHCachingImageManager is caching
            let options: PHImageRequestOptions = {
                let options = PHImageRequestOptions()
                options.deliveryMode = .HighQualityFormat
                options.resizeMode = .Fast
                options.synchronous = false
                
                return options
            }()
            
            if assetsToStartCaching != nil {
                imageManager.startCachingImagesForAssets(assetsToStartCaching!,
                                                         targetSize: targetSize,
                                                         contentMode: .AspectFill,
                                                         options: options)
            }
            
            if assetsToStopCaching != nil {
                imageManager.stopCachingImagesForAssets(assetsToStopCaching!,
                                                        targetSize: targetSize,
                                                        contentMode: .AspectFill,
                                                        options: nil)
            }
            
            // Store the preheat rect to compare against in the future
            previousPreheatRect = preheatRect
        }
    }
}

// MARK: - 3D Touch
extension PhotoPreviewViewController {
    @available(iOS 9.0, *)
    override func previewActionItems() -> [UIPreviewActionItem] {
        let likeAction = UIPreviewAction(title: "Like", style: .Default) { (action, viewController) in
            print("OK, I know you like it!")
        }
        
        let deleteAction = UIPreviewAction(title: "Delete", style: .Destructive) { (action, viewController) in
            print("OK, you want to delete but it's impossible")
        }
        
        return [likeAction, deleteAction]
    }
}
