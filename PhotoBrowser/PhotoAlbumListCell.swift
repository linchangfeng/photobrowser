//
//  PhotoAlbumListCell.swift
//  PhotoBrowser
//
//  Created by flowdev on 9/5/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class PhotoAlbumListCell: UITableViewCell {
    
    @IBOutlet weak var photoAlbumThumbnailImageView: UIImageView!
    @IBOutlet weak var photoAlbumTitleLabel: UILabel!
    @IBOutlet weak var photoAlbumCountLabel: UILabel!
}
