//
//  PhotoPreviewCollectionViewCell.swift
//  PhotoPreview
//
//  Created by flowdev on 9/2/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class PhotoPreviewCollectionViewCell: UICollectionViewCell {
    
    private let padding: CGFloat = 10.0
    
    var page: ZoomingScrollView
    var playButton = UIButton()
    
    var representedAssetIdentifier: String?
    
    override var backgroundColor: UIColor? {
        didSet {
            page.backgroundColor = backgroundColor
        }
    }
    
    override init(frame: CGRect) {
        page = ZoomingScrollView(frame: CGRect.zero)
        playButton.setImage(UIImage(named: "Play"), forState: .Normal)
        playButton.hidden = true
        
        super.init(frame: frame)
        
        contentView.addSubview(page)
        contentView.addSubview(playButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        page.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: frame.width - padding * 2.0, height: frame.height))
        
        playButton.frame = CGRectMake(0, 0, 40, 40)
        playButton.centerX = self.halfWidth
        playButton.centerY = self.halfHeight
    }
    
    override func prepareForReuse() {
        page.image = nil
        page.photoImageView.image = nil
        playButton.hidden = true
    }
    
}
